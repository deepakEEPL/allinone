import { AsyncStorage } from "react-native";
import { db } from "../Component/Config";
export const isUserLoggedIn = async () =>
  new Promise((resolve, reject) => {
    AsyncStorage.getItem("@PhoneNumber:key")
      .then(value => {
        const item = JSON.parse(value);
        if (item != null) {
          resolve("Registerd");
        } else {
          reject("Not registerd");
        }
      })
      .catch(err => {
        reject("Not registerd");
      });
  });

  export  const StoreUserPhoneNumber = async (phoneNumber) =>
  new Promise((resolve, reject) => {
    AsyncStorage.setItem("@PhoneNumber:key", phoneNumber)
      .then(result => {
        
        resolve(result);
      })
      .catch(err => {
        reject(err);
      });
  });
