export const RechargeData = [
    {
      text: "Paytm",
      image: require("../../Assets/paytm.png"),
      Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fpaytm.com"
    },
    {
      text: "MobiKwik",
      image: require("../../Assets/mobikwik.png"),
      Url: "https://www.mobikwik.com/"
    },
    {
      text: "Freecharge",
      image: require("../../Assets/freecharge.png"),
      Url: "https://www.freecharge.in/"
    },
    {
      text: "Airtel Recharge",
      image: require("../../Assets/airtelreacharge.png"),
      Url: "https://www.airtel.in/prepaid-recharge"
    },

    {
      text: "Jio Recharge",
      image: require("../../Assets/ticketnew.png"),
      Url: "https://www.jio.com/JioWebApp/index.html?root=primeRecharge"
    },

    {
      text: "Oxigen Wallet",
      image: require("../../Assets/oxigbuy.png"),
      Url: "https://www.oxigenwallet.com/mobile-recharge"
    },

    {
      text: "BSES",
      image: require("../../Assets/bses.png"),
      Url: "https://www.bsesdelhi.com/"
    },

    {
      text: "Tata Sky",
      image: require("../../Assets/tatasky.png"),
      Url: "https://www.mytatasky.com/web/portal/emailcampaign"
    }
  ];
