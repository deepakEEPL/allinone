export const couponCode = [
    {
        HeaderText: "Laptop Bonanza",
        DescriptionText: "26th-28th March",
        iconName: require('../../Assets/flipkart.png'),
        backGroundImage: require('../../Assets/flip_laptop.jpg'),
        link : "https://www.flipkart.com/laptop-sale-store?iid=M_f23d8beb-b506-4fe8-bd69-9113ae247b37_4.34B6XA48HIQ0&ppt=Homepage&affid=eyetech066&ppn=Homepage"
    },
   
    {
        HeaderText: "Top Rated TVs",
        DescriptionText: "Best price | Top Brands",
        iconName: require('../../Assets/flipkart.png'),
        backGroundImage: require('../../Assets/Flip_tv.jpg'),
        link : "https://www.flipkart.com/televisions/~limited-time-limited-stock/pr?p%5B%5D=facets.fulfilled_by%255B%255D%3DFlipkart%2BAssured&p%5B%5D=facets.availability%255B%255D%3DExclude%2BOut%2Bof%2BStock&p%5B%5D=facets.rating%255B%255D%3D4%25E2%2598%2585%2B%2526%2Babove&iid=M_f23d8beb-b506-4fe8-bd69-9113ae247b37_18.NBV4AXGBTBPN&ppt=Homepage&affid=eyetech066&sort=popularity&sid=ckf%2Cczl&ppn=Homepage"
    },
    {
        HeaderText: "Home products",
        DescriptionText: "Up to 75% Off",
        iconName: require('../../Assets/amazon.png'),
        backGroundImage: require('../../Assets/Amazon_Home-Products.jpg'),
        link : "https://www.amazon.in/home-products-sale/b/ref=as_li_ss_tl?ie=UTF8&node=12414705031&linkCode=sl2&tag=syedkhaja-21&linkId=83fe384262b560981f605b483f48e8d8&language=en_IN"
    },
    {
        HeaderText: "Baby Products",
        DescriptionText: "Up to $0% Off",
        iconName: require('../../Assets/amazon.png'),
        backGroundImage: require('../../Assets/Amazon_Baby.jpg'),
        link : "https://www.amazon.in/b/ref=as_li_ss_tl?node=14493916031&pf_rd_p=1783fa18-2cb4-4a3f-b9db-e73d33eeed86&pf_rd_r=92THVF2ZWKZBQ7T89MJD&linkCode=sl2&tag=syedkhaja-21&linkId=16a0d923870f2cbc055fca9ec972cdba&language=en_IN"
    },
    {
        HeaderText: "Redmi Go Sale Today, 2PM",
        DescriptionText: "Just  4,449",
        iconName: require('../../Assets/flipkart.png'),
        backGroundImage: require('../../Assets/Flip_mobile.jpg'),
        link : "https://www.flipkart.com/mobile-phones-store?param=23&iid=M_f23d8beb-b506-4fe8-bd69-9113ae247b37_4.T8WMSYSPSL21&ppt=Homepage&affid=eyetech066&ppn=Homepage"
    },
    {
        HeaderText: "Beauty & Grooming",
        DescriptionText: "Up to 40% Off",
        iconName: require('../../Assets/amazon.png'),
        backGroundImage: require('../../Assets/Amazon_Beauty.jpg'),
        link : "https://www.amazon.in/b/ref=as_li_ss_tl?node=6629031031&pf_rd_p=313cfd2d-167b-43fa-a74c-4e9bbac049c8&pf_rd_r=92THVF2ZWKZBQ7T89MJD&linkCode=sl2&tag=syedkhaja-21&linkId=6cc68aef153c20e1c610c4b8eb18ef58&language=en_IN"
    },
    {
        HeaderText: "Bring winter to your home",
        DescriptionText: "Up to 60% off",
        iconName: require('../../Assets/flipkart.png'),
        backGroundImage: require('../../Assets/flip_Ac.jpg'),
        link : "https://www.flipkart.com/cooling-days-store?affid=eyetech066"
    }
]