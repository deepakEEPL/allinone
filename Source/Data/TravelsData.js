export const TraverlsData = [
  {
    text: "OLA",
    image: require("../../Assets/ola.png"),
    Url: "https://www.olacabs.com/"
  },
  {
    text: "Uber",
    image: require("../../Assets/uber.png"),
    Url: "https://www.uber.com/en-IN/"
  },
  {
    text: "MakeMyTrip",
    image: require("../../Assets/make_my_trip.png"),
    Url: "https://ad.admitad.com/g/uu693psu230ca37638179814d2cd5d/?subid=khaja"
  },
  {
    text: "Yatra Flight Booking",
    image: require("../../Assets/yatra.png"),
    Url: "https://www.yatra.com/"
  },
  {
    text: "Cleartrip",
    image: require("../../Assets/cleartrip.png"),
    Url: "https://tracking.vcommission.com/aff_c?offer_id=2175&aff_id=89863&source=khaja"
  },
  {
    text: "Goibibo",
    image: require("../../Assets/go_ibibo.png"),
    Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fwww.goibibo.com%2Fflights%2F"
  },
  {
    text: "Expedia",
    image: require("../../Assets/expedia.png"),
    Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fwww.expedia.co.in%2F"
  },
  {
    text: "Trivago Flight and Hotel compare",
    image: require("../../Assets/trivago.png"),
    Url: "https://www.trivago.in/"
  },
  {
    text: "Via",
    image: require("../../Assets/via.png"),
    Url: "https://in.via.com/"
  },
  {
    text: "Ixigo",
    image: require("../../Assets/ixigo.png"),
    Url: "https://www.ixigo.com/"
  },
  {
    text: "OYO",
    image: require("../../Assets/oyo.png"),
    Url: "https://www.oyorooms.com/"
  },
  {
    text: "Yatra Hotel Booking",
    image: require("../../Assets/yatra.png"),
    Url: "https://ad.admitad.com/g/7c5o1da3vb0ca3763817756c7e13e2/"
  },
  {
    text: "MMT Hotel",
    image: require("../../Assets/makemytriphotels.png"),
    Url: "https://ad.admitad.com/g/uu693psu230ca37638179814d2cd5d/?subid=khaja"
  },
  {
    text: "Hotels",
    image: require("../../Assets/hotelscom.png"),
    Url: "https://clnk.in/hUgR"
  },
  {
    text: "Treebo",
    image: require("../../Assets/treebohotels.png"),
    Url: "https://ad.admitad.com/g/zyl3uhaoda0ca3763817a1f6c10897/?subid=khaja"
  },
  {
    text: "Trivogo Hotel compare",
    image: require("../../Assets/trivago.png"),
    Url: "https://www.trivago.in/"
  },
  {
    text: "Easy My Trip",
    image: require("../../Assets/emt.png"),
    Url: "https://www.easemytrip.com/"
  },
  {
    text: "IRCTC Train booking",
    image: require("../../Assets/irctc.png"),
    Url: "https://www.irctc.co.in/nget/train-search"
  },
  {
    text: "Paytm Train Bus Flight booking",
    image: require("../../Assets/paytm.png"),
    Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fpaytm.com"
  },
  {
    text: "RedBus",
    image: require("../../Assets/red_bus.png"),
    Url: "https://www.redbus.in/"
  },
  {
    text: "MyBusTickets",
    image: require("../../Assets/mybustickets.png"),
    Url: "https://www.mybustickets.in/"
  },
  {
    text: "TicketGoose",
    image: require("../../Assets/ticketgoose.png"),
    Url: "https://www.ticketgoose.com/"
  },{
    text: "ZoomCar",
    image: require("../../Assets/zoomcar.png"),
    Url: "https://www.zoomcar.com/"
  }
];
