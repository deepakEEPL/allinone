export const CarData = [
    {
      text: "CarTrade",
      image: require("../../Assets/car_trade.png"),
      Url: "https://www.cartrade.com/"
    },
    {
      text: "CarDekho",
      image: require("../../Assets/car_dekho.png"),
      Url: "https://www.cardekho.com/write-car-user-review.html"
    },
    {
        text: "OverDrive",
        image: require("../../Assets/overdrive.png"),
        Url: "http://overdrive.in/"
      },
      {
        text: "Gaddi",
        image: require("../../Assets/gaadi.png"),
        Url: "https://www.gaadi.com/"
      },
      {
        text: "ZigWhees",
        image: require("../../Assets/zigwheels.png"),
        Url: "https://www.zigwheels.com/"
      },
      {
        text: "Amazon Car Purchase",
        image: require("../../Assets/amazon.png"),
        Url: "https://www.amazon.in/s/ref=as_li_ss_tl?k=car+accessories&crid=2JE4ND2EBJYU6&sprefix=car+a,aps,390&ref=nb_sb_ss_i_2_5&linkCode=ll2&tag=syedkhaja-21&linkId=4385989352e78a34bcdede7fcde2e8da&language=en_IN"
      }
  ];
  