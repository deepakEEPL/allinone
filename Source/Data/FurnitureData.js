export const FurnitureData = [
  {
    text: "PepperFry",
    image: require("../../Assets/pepper_fry.png"),
    Url: "https://ad.admitad.com/g/3a063wka0y0ca37638176816d5588c/?subid=khaja"
  },
  {
    text: "UrbanLadder",
    image: require("../../Assets/urban_ladder.png"),
    Url: "https://www.urbanladder.com/"
  },
  {
    text: "Flipkart Furniture",
    image: require("../../Assets/flipkart.png"),
    Url: "http://fkrt.it/pyYeLnuuuN"
  },
  {
    text: "Amazon Furniture",
    image: require("../../Assets/amazon.png"),
    Url: "https://www.amazon.in/s/ref=as_li_ss_tl?url=search-alias=furniture&field-keywords=&linkCode=ll2&tag=syedkhaja-21&linkId=1ff821c8c7b910d43c1740d75744d335&language=en_IN"
  }
];
