export const MovieData = [
    {
        text: "BookMyShow",
        image: require("../../Assets/book_my_show.png"),
        Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fin.bookmyshow.com%2F"
      },
      {
        text: "INOX",
        image: require("../../Assets/inox.png"),
        Url: "https://www.inoxmovies.com/"
      },
      {
        text: "PVR Cinemas",
        image: require("../../Assets/pvr.png"),
        Url: "https://www.pvrcinemas.com/"
      },
      {
        text: "Paytm",
        image: require("../../Assets/paytm.png"),
        Url: "https://paytm.com/movies"
      },
      {
        text: "TicketNew",
        image: require("../../Assets/ticketnew.png"),
        Url: "https://www.ticketnew.com/Movie-Ticket-Online-booking/C/Chennai"
      }
      
]