export const shoppingData = [
    {
      text: "Amazom",
      image: require("../../Assets/amazon.png"),
      Url: "https://www.amazon.in/ref=as_li_ss_tl?ie=UTF8&linkCode=ll2&tag=syedkhaja-21&linkId=31479f8d97f1473fc2f70501d7bd7cc3&language=en_IN"
    },
    {
      text: "Banggood",
      image: require("../../Assets/bangood.png"),
      Url: "https://ad.admitad.com/g/e8f129b05e0ca37638176213826a88/?subid=khaja"
    },
    {
      text: "Flipkart",
      image: require("../../Assets/flipkart.png"),
      Url: "http://fkrt.it/zdipBnuuuN"
    },
    {
      text: "AliExpress",
      image: require("../../Assets/AliExpress.png"),
      Url: "http://ali.pub/35vqey"
    },
    {
      text: "Myntra",
      image: require("../../Assets/myntra.png"),
      Url: "https://tracking.vcommission.com/aff_c?offer_id=22&aff_id=89863&source=khaja"
    },
    {
      text: "Jabong",
      image: require("../../Assets/jabong.png"),
      Url: "https://www.jabong.com/"
    },
    {
      text: "Shopclues",
      image: require("../../Assets/shopclues.png"),
      Url: "http://tracking.vcommission.com/aff_c?offer_id=122&aff_id=89863&source=khaja"
    },
    {
      text: "Paytm Mall",
      image: require("../../Assets/paytmmall.png"),
      Url: "https://ad.admitad.com/g/skgi6a1tct0ca37638176339fea834/?subid=khaja"
    },
    {
      text: "Voonik",
      image: require("../../Assets/voonik.png"),
      Url: "https://urlzs.com/4spF"
    },
    {
      text: "Mr Vooonik",
      image: require("../../Assets/mrvoonik.png"),
      Url: "https://www.voonik.com/collections/men-favorites?lp=true"
    },
    {
      text: "Ebay",
      image: require("../../Assets/ebay.png"),
      Url: "https://www.ebay.com/"
    },
    {
      text: "TATA",
      image: require("../../Assets/tatasky.png"),
      Url: "https://clnk.in/hOWD/"
    },
    {
      text: "Ajio",
      image: require("../../Assets/ajio.png"),
      Url: "https://www.ajio.com/"
    },
    {
      text: "Snapdeal",
      image: require("../../Assets/snapdeal.png"),
      Url: "https://www.snapdeal.com/"
    },
    {
      text: "Shoppers Stop",
      image: require("../../Assets/shoppersstop.png"),
      Url: "https://www.shoppersstop.com/"
    },
    {
      text: "Big Bazaar",
      image: require("../../Assets/bigbazaar.png"),
      Url: "https://www.bigbazaar.com/"
    },
    {
      text: "Max",
      image: require("../../Assets/max.png"),
      Url: "https://www.maxfashion.in/in/en/"
    },
    {
      text: "My Vishal",
      image: require("../../Assets/vishalmegamart.png"),
      Url: "https://myvishal.com/"
    },
    {
      text: "Adof",
      image: require("../../Assets/adof.png"),
      Url: "https://www.abof.com/"
    },
    {
      text: "NNNOW",
      image: require("../../Assets/nnnow.png"),
      Url: "https://tracking.vcommission.com/aff_c?offer_id=2978&aff_id=89863&source=khaja"
    },
    {
      text: "Fynd",
      image: require("../../Assets/gofynd.png"),
      Url: "https://www.fynd.com/"
    },
    {
      text: "Infibeam",
      image: require("../../Assets/infibeam.png"),
      Url: "https://www.infibeam.com/"
    },
    {
      text: "Yepme",
      image: require("../../Assets/yepme.png"),
      Url: "http://www.yepme.com/"
    },
    {
      text: "Naaptol",
      image: require("../../Assets/naaptol.png"),
      Url: "https://www.naaptol.com/"
    },
    {
      text: "Homeshop 18",
      image: require("../../Assets/homeshop18.png"),
      Url: "https://www.homeshop18.com/"
    },
    {
      text: "Rediff",
      image: require("../../Assets/rediff.png"),
      Url: "http://www.rediff.com/"
    },
    {
      text: "JOCKEY",
      image: require("../../Assets/assets_jockey.png"),
      Url: "https://ad.admitad.com/g/tzj7koxn9z0ca3763817e01e0df27d/?subid=khaja"
    },
    {
      text: "FashionYou",
      image: require("../../Assets/fashion_and_you.png"),
      Url: "https://testfay.myshopify.com/"
    },
    {
      text: "ShopBAZAAR",
      image: require("../../Assets/shop_bazaar.png"),
      Url: "https://shop.harpersbazaar.com/"
    }
  ]