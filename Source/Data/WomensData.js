export const WomensData = [
    {
        text: "Voonik",
        image: require("../../Assets/voonik.png"),
        Url: "https://www.voonik.com/collections/women-fashion?lp=true"
      },
      {
        text: "Zivame",
        image: require("../../Assets/zivame.png"),
        Url: "https://ad.admitad.com/g/t2nm6m1kgc0ca37638175f08f4c55f/"
      },
      {
        text: "Limeroad",
        image: require("../../Assets/limeroad.png"),
        Url: "https://tracking.vcommission.com/aff_c?offer_id=280&aff_id=89863&source=khaja"
      },{
        text: "Nykaa",
        image: require("../../Assets/nykaa.png"),
        Url: "https://tracking.vcommission.com/aff_c?offer_id=232&aff_id=89863&source=khaja"
      },
      {
        text: "Purplle",
        image: require("../../Assets/purplle.png"),
        Url: "https://www.purplle.com/"
      },
      {
        text: "Biba",
        image: require("../../Assets/biba.png"),
        Url: "https://ad.admitad.com/g/4gxko08abw0ca3763817f2e59b1932/"
      },{
        text: "Craftsvilla",
        image: require("../../Assets/craftsvilla.png"),
        Url: "https://www.craftsvilla.com/"
      },
      {
        text: "STALK BUY LOVE",
        image: require("../../Assets/stalkbuylove.png"),
        Url: "https://ad.admitad.com/g/h9eteooiei0ca3763817a412176801/?subid=khaja"
      },
      {
        text: "FABALLEY",
        image: require("../../Assets/faballey.png"),
        Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fwww.faballey.com%2F"
      },
      {
        text: "Clovia",
        image: require("../../Assets/clovia.png"),
        Url: "https://tracking.vcommission.com/aff_c?offer_id=146&aff_id=89863&source=khaja"
      },
      {
        text: "Chumbak",
        image: require("../../Assets/chumbak.png"),
        Url: "https://ad.admitad.com/g/8522h1jqqo0ca376381751514c771c/?subid=khaja"
      },
      {
        text: "PreetySecrets",
        image: require("../../Assets/prettysecrets.png"),
        Url: "https://tracking.vcommission.com/aff_c?offer_id=3130&aff_id=89863&source=khaja"
      },
      {
        text: "WforWoman",
        image: require("../../Assets/wforwomen.png"),
        Url: "https://wforwoman.com/"
      },
      {
        text: "Bluestone",
        image: require("../../Assets/bluestone.png"),
        Url: "https://tracking.vcommission.com/aff_c?offer_id=713&aff_id=89863&source=khaja"
      },
      {
        text: "Voylla",
        image: require("../../Assets/voylla.png"),
        Url: "https://tracking.vcommission.com/aff_c?offer_id=1686&aff_id=89863&source=khaja"
      },
      {
        text: "Flipkart",
        image: require("../../Assets/flipkart.png"),
        Url: "http://fkrt.it/rDN7LKNNNN"
      },
      {
        text: "Amazon",
        image: require("../../Assets/amazon.png"),
        Url: "https://www.amazon.in/www.amazon.in/b/ref=as_li_ss_tl?node=7459780031&linkCode=ll2&tag=syedkhaja-21&linkId=d26e5c48f3c2644bd682b3c0c359d831&language=en_IN"
      }
]