export const GroceryData = [
    {
      text: "Bigbasket",
      image: require("../../Assets/bigbasket.png"),
      Url: "https://ad.admitad.com/g/gob3zzel350ca37638171051f25b5d/?subid=khaja"
    },
    {
      text: "Amazon Grocery",
      image: require("../../Assets/amazon.png"),
      Url: "https://www.amazon.in/s/ref=as_li_ss_tl?url=search-alias=grocery&field-keywords=&linkCode=ll2&tag=syedkhaja-21&linkId=29668837c810d3bd38ae982414a68cd8&language=en_IN"
    },
    {
      text: "Patanjali",
      image: require("../../Assets/patanjaliayurved.png"),
      Url: "https://www.patanjaliayurved.net/"
    },
    {
      text: "Grofers",
      image: require("../../Assets/grofers.png"),
      Url: "https://grofers.com/"
    },
    {
      text: "Natures Basket",
      image: require("../../Assets/nature_basket.png"),
      Url: "http://www.naturesbasket.co.in/"
    },
    {
      text: "Justdial",
      image: require("../../Assets/justdial.png"),
      Url: "https://www.justdial.com/Grocery"
    },
    {
      text: "Big Bazaar",
      image: require("../../Assets/bigbazaar.png"),
      Url: "https://www.bigbazaar.com/"
    }
]
    