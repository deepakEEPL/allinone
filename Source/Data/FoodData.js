export const FoodData = [
  {
    text: "Zomato",
    image: require("../../Assets/zomato.png"),
    Url: "https://www.zomato.com"
  },
  {
    text: "McDonalds",
    image: require("../../Assets/mcdonalds.png"),
    Url: "https://ad.admitad.com/g/amg5q1j3lg0ca37638170e6a7d725a/?subid=khaja"
  },
  {
    text: "Dominos Pizza",
    image: require("../../Assets/dominos.png"),
    Url: "https://pizzaonline.dominos.co.in/"
  },
  {
    text: "Haldirams",
    image: require("../../Assets/haldiram.png"),
    Url: "http://www.haldirams.com/"
  },
  {
    text: "Foodpanda",
    image: require("../../Assets/foodpanda.png"),
    Url: "https://www.foodpanda.in/"
  },
  {
    text: "KFC",
    image: require("../../Assets/kfc.png"),
    Url: "https://linksredirect.com/?pub_id=36112CL32581&subid=Khaja&source=linkkit&url=https%3A%2F%2Fonline.kfc.co.in%2Fhome"
  },
  {
    text: "Pizza Hut",
    image: require("../../Assets/pizza_hut.png"),
    Url: "https://tracking.vcommission.com/aff_c?offer_id=442&aff_id=89863&source=khaja"
  },
  {
    text: "Just Eat",
    image: require("../../Assets/assets_just_eat.png"),
    Url: "https://www.just-eat.com/"
  },
  {
    text: "Swiggy",
    image: require("../../Assets/swiggy.png"),
    Url: "https://ad.admitad.com/g/ta366jiz5o0ca376381774c5fc7d3a/?subid=khaja"
  },
  {
    text: "TravelKhana",
    image: require("../../Assets/travelkhana.png"),
    Url: "https://www.travelkhana.com/"
  },
  {
    text: "LittleApp",
    image: require("../../Assets/littleapp.png"),
    Url: "https://littleapp.in/"
  },
  {
    text: "FreshMenu",
    image: require("../../Assets/freshmenu.png"),
    Url: "https://www.freshmenu.com/"
  }
];
