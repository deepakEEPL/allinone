import { Header, Left, Body, Right, Button, Icon, Title } from "native-base";
import React, { Component } from "react";
import { Image, Share } from "react-native";
import { DrawerActions } from "react-navigation";
export default (HeaderComponent = props => {
  return (
    <Header style={{ backgroundColor: "#fff" }}>
      <Left>
        <Button
          transparent
          onPress={() => {
            props.navigation.dispatch(DrawerActions.toggleDrawer());
          }}
        >
          <Icon name="menu" style={{ color: "#000" }} />
        </Button>
      </Left>
      <Body>
        <Title
          adjustsFontSizeToFit
          style={{
            color: "#000",
            alignSelf: "flex-start"
          }}
        >
          {props.name}
        </Title>
      </Body>
      <Right>
        <Button transparent onPress = {()=> {
          props.navigation.push("SearchPresenter");
         }}>
          <Image
            source={require("../../Assets/searchonclick.png")}
            style={{ height: 25, width: 25 }}
          />
        </Button>
        <Button transparent onPress = {()=>{
           onShare()
        }}>
          <Image
            source={require("../../Assets/ic_share_onclick.png")}
            style={{ height: 25, width: 25 }}
          />
        </Button>
        <Button transparent>
          <Image
            source={require("../../Assets/dealonclick.png")}
            style={{ height: 25, width: 25 }}
          />
        </Button>
      </Right>
    </Header>
  );
});

onShare = async () => {
  try {
    const result = await Share.share({
      message:
        "All in One app",
    });

    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) {
    alert(error.message);
  }
}
