import React, { Component } from "react";
import { Text, View, ListView } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { Button, Title } from "native-base";
import CouponCodeCell from "./CouponCodeCell";
import { couponCode } from "../Data/CouponCode";

export default class CouponCodeListViewProvider extends Component {
  constructor() {
    super();
    this.state = {
      slider1ActiveSlide: 0,
      pageWidth: 1,
      pageHeight: 1
    };
  }

  onCellClickWithItem(link, HeaderText) {
    this.props.navigationvalue.navigate("WebPresenter", {
      url: link,
      name: HeaderText
    });
  }

  _renderItem({ item, index }) {
    const {
      HeaderText,
      DescriptionText,
      iconName,
      backGroundImage,
      link
    } = item;
    return (
      <CouponCodeCell
        HeaderText={HeaderText}
        DescriptionText={DescriptionText}
        iconName={iconName}
        backGroundImage={backGroundImage}
        onCellClicked={() => {
          this.onCellClickWithItem(link, HeaderText);
        }}
      />
    );
  }
  getCarousel() {
    return (
      <Carousel
        data={couponCode}
        renderItem={this._renderItem.bind(this)}
        // loop = {true}
        onSnapToItem={index => this.setState({ slider1ActiveSlide: index })}
        sliderWidth={this.state.pageWidth}
        itemWidth={this.state.pageWidth * 0.7}
        itemHeight={this.state.pageHeight}
      />
    );
  }

  render() {
    return (
      <View
        style={{ flex: 1 }}
        onLayout={event => {
          this.setState({
            pageWidth: event.nativeEvent.layout.width,
            pageHeight: event.nativeEvent.layout.height
          });
        }}
      >
        {this.getCarousel()}
      </View>
    );
  }
}
