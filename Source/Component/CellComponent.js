import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Button } from "native-base";

export default class CellComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cellWidth: 0,
      cellHeight: 0
    };
  }

  render() {
    return (
      <View
        onLayout={event => {
          var width = event.nativeEvent.layout.width;
          if (this.state.cellWidth != width) {
            this.setState({ cellWidth: width });
          }
          if (this.state.cellHeight != width) {
            this.setState({ cellHeight: width });
          }
        }}
      >
        <Button
          style={{ width: this.state.cellWidth, height: this.state.cellHeight , backgroundColor: '#fff'}}
          onPress={() => {
            console.log("pressing");
            this.props.onCellClicked();
          }}
        >
          {this.getGridView()}
        </Button>
      </View>
    );
  }

  getGridView() {
    if (this.state.cellWidth > 0) {
      return (
        <View
          style={{
            width: this.state.cellWidth,
            height: this.state.cellHeight,
            backgroundColor: "#fff",
            borderRadius: 5,
            borderColor: "#f2f2f2",
            borderWidth: 0.5,
            shadowColor: "#000",
            shadowOffset: {
              width: 2,
              height: 1
            },
            shadowRadius: 2,
            shadowOpacity: 0.5,
            padding: 8,
            elevation: 4,
            zIndex: 999
          }}
        >
          <Image
            source={this.props.image}
            style={{ width: null, height: this.state.cellHeight * 0.6 }}
            resizeMode="contain"
          />
          <Text
            style={{
              textAlign: "left",
              padding: 8,
              color: "#000",
              fontSize: 14
            }}
          >
            {this.props.cellName}
          </Text>
        </View>
      );
    }
  }
}
