import React, { Component } from "react";
import { Text, View, ListView } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import CategoryCell from "./CategoryCell";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Button } from "native-base";

const data = [
  {
    name: "Women's",
    image: require("../../Assets/Womens.jpg"),
    link : "https://amzn.to/2FRVvE0"
  },
  {
    name: "Men and Women",
    image: require("../../Assets/shopping-hero-first_card.jpg"),
    link : "https://amzn.to/2FRciXD"
  }
];

const data1 = [
  {
    name: "Baby and kids",
    image: require("../../Assets/BabyShopping4.jpg"),
    link : "https://www.firstcry.com"
  }
];

export default class CategoryListProvider extends Component {
  constructor() {
    super();
    this.state = {
      slider1ActiveSlide: 0,
      pageWidth: 1,
      pageHeight: 1
    };
  }

  onCellClickWithItem(link, HeaderText) {
    this.props.navigationvalue.navigate("WebPresenter", {
                url: link,
                name: HeaderText
              });
    }

    
  _renderItem({ item, index }) {
    const { name, image, link } = item;
    return (
      <CategoryCell name={name} image={image} onCellPressed = {()=> {
        this.onCellClickWithItem(link, name)
      }} />
    );
  }
  getCarousel() {
    return (
      <Carousel
        data={this.props.nameValue == 1 ? data : data1}
        renderItem={this._renderItem.bind(this)}
        onSnapToItem={index => this.setState({ slider1ActiveSlide: index })}
        sliderWidth={this.state.pageWidth}
        itemWidth={250}
        itemHeight={114}
      />
    );
  }

  render() {
    return (
      <View
        style={{ flex: 1 }}
        onLayout={event => {
          this.setState({
            pageWidth: event.nativeEvent.layout.width,
            pageHeight: event.nativeEvent.layout.height
          });
        }}
      >
        {this.getCarousel()}
      </View>
    );
  }
}
