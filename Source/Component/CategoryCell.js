import React, { Component } from "react";
import { Text, View, Image, StyleSheet } from "react-native";
import { Container, Button } from "native-base";
import {
  TouchableOpacity,
  TouchableHighlight
} from "react-native-gesture-handler";

export default class CategoryCell extends Component {
  constructor() {
    super();
    this.state = {
      pageWidth: 1,
      pageHeight: 1
    };
  }
  render() {
    return (
      <Container style={{ padding: 10, backgroundColor: "transparent" }}>
        <Container
          style={styles.containerStyle}
          onLayout={event => {
            this.setState({
              pageWidth: event.nativeEvent.layout.width,
              pageHeight: event.nativeEvent.layout.height
            });
          }}
        >
          <Button transparent
            style={{
              width: this.state.pageWidth - 16,
              height: this.state.pageHeight - 24,
              paddingBottom: 8
            }}
            onPress={() => {
              this.props.onCellPressed();
            }}
          >
            {this.getNameAndIcon()}
          </Button>
        </Container>
      </Container>
    );
  }

  getNameAndIcon() {
    return (
      <Container
        style={{
          width: this.state.pageWidth - 16,
          height: this.state.pageHeight - 16
        }}
      >
        <Image
          source={this.props.image}
          style={{ height: this.state.pageHeight * 0.6, width: this.state.pageWidth - 16, alignSelf: "flex-end" }}
        />
        <Container
          style={{
            flexDirection: "column",
            padding : 10,
            height: 80,
            width: this.state.pageWidth - 16,
            backgroundColor: "transparent"
          }}
        >
          <Text style={{ color: "#000", fontSize: 18 }} numberOfLines={3}>
            {this.props.name}
          </Text>
          <Text style={{ color: "#000", fontSize: 14 }}>
            {"Shopping"}
          </Text>
        </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: "#fff",
    borderRadius: 5,
    borderColor: "#f2f2f2",
    borderWidth: 0.5,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5,
    padding: 8,
    elevation: 4,
    zIndex: 999
  }
});
