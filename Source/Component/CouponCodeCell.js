import React, { Component } from "react";
import { Text, View, Image, StyleSheet } from "react-native";
import { Container, Button } from "native-base";

export default class CouponCodeCell extends Component {
  constructor() {
    super();
    this.state = {
      pageWidth: 1,
      pageHeight: 1
    };
  }
  render() {
    return (
      <Container style={{ padding: 10, backgroundColor: "transparent" }}>
        <Container
          style={styles.containerStyle}
          onLayout={event => {
            this.setState({
              pageWidth: event.nativeEvent.layout.width,
              pageHeight: event.nativeEvent.layout.height
            });
          }}
        >
          <Button transparent
            style={{
              width: this.state.pageWidth - 16,
              height: this.state.pageHeight - 24,
              paddingBottom: 8
            }}
            onPress={() => {
              this.props.onCellClicked();
            }}
          >
            {this.getBackgroundImageWithIcons()}
          </Button>
        </Container>
      </Container>
    );
  }
  getBackgroundImageWithIcons() {
    return (
      <Container
        style={{
          flexDirection: "column",
          width: this.state.pageWidth - 16,
          height: this.state.pageHeight - 16,
          alignSelf: "flex-start"
        }}
      >
        <Image
          resizeMode="contain"
          source={this.props.backGroundImage}
          style={{
            height: 150,
            width: this.state.pageWidth - 16,
            alignSelf: "flex-end"
          }}
        />
        {this.getNameAndIcon()}
      </Container>
    );
  }
  getNameAndIcon() {
    return (
      <Container
        style={{
          flexDirection: "row",
          width: this.state.pageWidth - 16,
          height: this.state.pageHeight - 16
        }}
      >
        <Image
          source={this.props.iconName}
          style={{ height: 80, width: 80, alignSelf: "flex-end" }}
        />
        <Container
          style={{
            flexDirection: "column",
            alignSelf: "flex-end",
            paddingLeft: 10,
            height: 80,
            width: this.state.pageWidth - 100,
            backgroundColor: "transparent"
          }}
        >
          <Text style={{ color: "#000", fontSize: 18 }} numberOfLines={3}>
            {this.props.HeaderText}
          </Text>
          <Text style={{ color: "#000", fontSize: 14 }}>
            {this.props.DescriptionText}
          </Text>
        </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: "#fff",
    borderRadius: 5,
    borderColor: "#f2f2f2",
    borderWidth: 0.5,
    shadowColor: "#000",
    shadowOffset: {
      width: 2,
      height: 1
    },
    shadowRadius: 2,
    shadowOpacity: 0.5,
    padding: 8,
    elevation: 4,
    zIndex: 999
  }
});
