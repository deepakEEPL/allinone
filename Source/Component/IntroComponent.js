import React, { Component } from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import {  } from "native-base";

export default class IntroComponent extends Component {
  render() {
    return (
      <View style={styles.containerStyle}>
        <Image style={styles.imageStye} source = {require("../../Assets/Icon.jpeg")} />
        <Text style={styles.textStyle}>{this.props.name}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    height: 100,
    backgroundColor: "#fff",
    padding: 16,
    alignItems: "center",
    flexDirection: "row"
  },
  imageStye: {
    height: 60,
    width: 60,
    borderRadius: 30,
  },
  textStyle: { padding: 16, fontSize: 20, fontWeight: "500" }
});
