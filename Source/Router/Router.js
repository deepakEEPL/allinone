import React, { Component } from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { DrawerActions } from "react-navigation";
import DrawerNavigator from "./DrawerNavigator";
import WebPresenter from './WebPresenter'
import {  Header, Left, Body, Right, Button, Icon, Title } from 'native-base';

const AppStack = createStackNavigator(
  {
    stackNavigator: {
      screen: DrawerNavigator,
      navigationOptions: ({ navigation }) => ({
        
      })
    } 
  }
  , {
      headerMode: 'none'
  }
);

export default createAppContainer(AppStack);
