import React, { Component } from "react";
import { Text, View } from "react-native";
import { DrawerActions } from 'react-navigation';
import Router from "./Router";
import {  Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
export default class Navigator extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Router />
      </View>
    );
  }
}
