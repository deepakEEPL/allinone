import React, { Component } from "react";
import { Text, View, WebView, SafeAreaView } from "react-native";
import { TabNavigator } from "./TabNavigator";
import Webbrowser from "react-native-custom-webview";
class Webpresenter extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: `${navigation.state.params.name}`,
      headerTintColor: '#000',
      drawerLabel: 'navigationOptions',
    };
  };
  get name() {
    return this.name;
  }
  render() {
    
    const { navigation } = this.props;
    const Url = navigation.getParam("url", "NO-ID");
    console.log(Url);
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <Webbrowser
          url={Url}
          hideHomeButton={false}
          hideToolbar={false}
          hideAddressBar={true}
          hideStatusBar={true}
          backButtonVisible={false}
          onBackPress={() => {
            goBack();
          }}
          foregroundColor="#000"
          backgroundColor="#fff"
        />
      </SafeAreaView>
    );
  }
}
export default Webpresenter;
