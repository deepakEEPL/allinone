import React, { Component } from "react";
import { Text, View,Image } from "react-native";
import Drawer from "../Screens/Drawer";
import {
  createDrawerNavigator,
  createStackNavigator,
  DrawerActions
} from "react-navigation";
import { tabStackNavigator } from "./TabNavigator";
import Webpresenter from "./WebPresenter";
import CouponCodeContainer from "../Screens/CouponCodeScreen";
import { Icon } from "native-base";
import FavouritesScreen from "../Screens/FavouritesScreen";
import RateAppScreen from "../Screens/RateAppScreen";
import ShareAppScreen from "../Screens/ShareAppScreen";
import CategoryScreen from "../Screens/CategoryScreen"
import PrivecyPolicyScreen from "../Screens/PrivecyPolicyScreen";
import ContactUsScreen from "../Screens/ContactUsScreen";
const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: tabStackNavigator,
      navigationOptions: ({ navigation }) => ({
        drawerIcon: ({tintColor}) => (<Icon name={"home"} style={{ color: tintColor }} />)
      })
    },
    CouponCode: {
      screen: CouponCodeContainer,
      navigationOptions: ({ navigation }) => ({
        drawerIcon: <Image source = {require('../../Assets/ic_stat_onesignal_default.png')} style = {{height : 30, width: 30}} />,
        headerTintColor: '#fff'
        
      })
    },
    Category : {
      screen: CategoryScreen,
      navigationOptions: ({ navigation }) => ({
        drawerIcon:  <Image source = {require('../../Assets/ic_category.png')} style = {{height : 30, width: 30}} />
      })
    },
    
    "Privecy Policy" : {
      screen: PrivecyPolicyScreen,
      navigationOptions: ({ navigation }) => ({
        drawerIcon: <Image source = {require('../../Assets/ic_privacy.png')} style = {{height : 30, width: 30}} />
      })
    },
    "Contact Us " : {
      screen: ContactUsScreen,
      navigationOptions: ({ navigation }) => ({
        drawerIcon: <Image source = {require('../../Assets/ic_privacy.png')} style = {{height : 30, width: 30}} />
      })
    }
  },
  {
    intialRouteName: "CouponCode",
    contentComponent: Drawer,
    drawerWidth: 300,
    contentOptions : {
      activeTintColor : '#fff',
      inactiveTintColor : '#eee'
    }
  }
);

export default DrawerNavigator;
