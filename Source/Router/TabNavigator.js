import React, { Component } from "react";
import {
  createMaterialTopTabNavigator,
  createStackNavigator,
  DrawerActions
} from "react-navigation";
import {
  Shopping,
  Womens,
  Recharge,
  Movie,
  BabyAndKids,
  Travel,
  Hotel,
  Food,
  Grocery,
  Medicine,
  Furniture,
  Classified,
  HomeService,
  car,
  Finance,
  Printing
} from "../Screens/Shopping";
import Webpresenter from "./WebPresenter";
import HeaderCommonComponent from "../Component/HeaderCommonComponent";
import SearchPresenter from "../Screens/SearchPresenter";


const TabNavigator = createMaterialTopTabNavigator(
  {
    Shopping: { screen: Shopping },
    Women: { screen: Womens },
    Recharge: { screen: Recharge },
    Movie: { screen: Movie },
    "Baby And Kids": { screen: BabyAndKids },
    Hotels: { screen: Hotel },
    Travels: { screen: Travel },
    Food: { screen: Food },
    Grocery: { screen: Grocery },
    Medicine: { screen: Medicine },
    Furniture: { screen: Furniture },
    Classified: { screen: Classified },
    HomeService: { screen: HomeService },
    Car: { screen: car },
    Finance: { screen: Finance },
    Printing: { screen: Printing }
  },
  {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: "#E91E63",
      inactiveTintColor: "black",
      indicatorStyle: {
        backgroundColor: "#E91E63"
      },
      scrollEnabled: true,
      labelStyle: {
        fontSize: 14,
        fontWeight: "600"
      },
      tabStyle: {
        height: 48,
        alignItems: "center",
        justifyContent: "center"
      },
      style: {
        backgroundColor: "white"
      },
      statusBarStyle: "light-content"
    }
  }
);

export const tabStackNavigator = createStackNavigator({
  TabNavigator: {
    screen: TabNavigator,
    navigationOptions: ({ navigation }) => ({
      header: (
        <HeaderCommonComponent
          navigation={navigation}
          name="All in one shopping"
        />
      )
    })
  },
  WebPresenter: { screen: Webpresenter },
  SearchPresenter: { screen: SearchPresenter }
});
