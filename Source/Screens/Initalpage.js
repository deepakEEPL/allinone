//import liraries
import React, { Component } from "react";
import { ActivityIndicator, View, Text, StyleSheet } from "react-native";
import WelcomeScreen from "./WelcomeScreen";
import Navigator from "../Router/Navigator";
import { isUserLoggedIn } from "../LocalDB/StorageUtils";

// create a component
class InitalPage extends Component {
  state = { isUserLoggedIn: "Loading" };
  render() {
    return this.getScreenStatus()
  }
  getScreenStatus() {
    if (this.state.isUserLoggedIn == "Loading") {
      return this.getLoadingScreen();
    } else if (this.state.isUserLoggedIn == "Not registered") {
      return <WelcomeScreen />;
    } else {
      return <Navigator />;
    }
  }
  getLoadingScreen() {
    isUserLoggedIn().then((result) => {
        this.setState({isUserLoggedIn: "Registered"})
    }).catch((err) => {
        this.setState({isUserLoggedIn: "Not registered"})
    });
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator />
      </View>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#2c3e50"
  }
});

//make this component available to the app
export default InitalPage;