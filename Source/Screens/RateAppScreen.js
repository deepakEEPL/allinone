import React, { Component } from "react";
import { Text, View, SafeAreaView } from "react-native";
import HeaderCommonComponent from "../Component/HeaderCommonComponent";
import { AirbnbRating } from "react-native-ratings";
import { Button, Title, Container } from "native-base";
import { DrawerActions } from "react-navigation";
import Modal from "react-native-modal";

export default class RateAppScreen extends Component {
  state = { rateVisible: true };
  componentWillMount() {
    this.setState({ rateVisible: true });
  }
  render() {
    return (
      <View>
        <HeaderCommonComponent
          navigation={this.props.navigation}
          name="Rate App"
        />
        {this.getAlert()}
        <View style={{ paddingTop: 40, alignItems: "center", padding: 16 }}>
          {this.getAlert()}
          <Button
            transparent
            style={{ alignSelf: "stretch", justifyContent: "center" }}
            onPress={() => {
              this.setState({ rateVisible: true });
            }}
          >
            <Title style={{ color: "#000" }}>Rate</Title>
          </Button>
        </View>
      </View>
    );
  }
  getAlert() {
    return (
      <Modal isVisible={this.state.rateVisible}>
        <View
          style={{
            paddingTop: 40,
            alignItems: "center",
            padding: 16,
            backgroundColor: "#fff"
          }}
        >
          <Text style={{ fontSize: 30, fontWeight: "600", padding: 10 }}>
            Rate out App
          </Text>
          <Text style={{ fontSize: 18, fontWeight: "600", paddingBottom: 30 }}>
            Let us know how we're doing. Please rate your experince using All in
            One Shopping App
          </Text>
          <AirbnbRating showRating={false} />
          <View style={{ height: 40 }} />
          <Button
            transparent
            style={{ alignSelf: "stretch", justifyContent: "center" }}
            onPress={() => {
              // this.setState({ rateVisible: false });
              this.props.navigation.dispatch(DrawerActions.toggleDrawer());
            }}
          >
            <Title style={{ color: "#000" }}>OK</Title>
          </Button>
        </View>
      </Modal>
    );
  }
}
