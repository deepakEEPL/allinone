import React, { Component } from "react";
import { Text, View, StyleSheet, Image } from "react-native";
import {
  Container,
  Item,
  Label,
  Input,
  CheckBox,
  Body,
  Title,
  Button
} from "native-base";
import { StoreUserPhoneNumber } from "../LocalDB/StorageUtils";
import firebase from '@firebase/app';
export default class UserDetailsScreen extends Component {
  state = { checkBoxStatus: false, number: "" };
  constructor(props) {
    super(props);
    let config = {
      apiKey: "AIzaSyAlUnSDXl0YVJfA_MvfATuOAQIhf8wUXdQ",
      authDomain: "all-in-one-shopping-app-f3f56.firebaseapp.com",
      databaseURL: "https://all-in-one-shopping-app-f3f56.firebaseio.com/",
      projectId: "all-in-one-shopping-app-f3f56",
      storageBucket: "gs://all-in-one-shopping-app-f3f56.appspot.com",
      messagingSenderId: "75400016157"
    };
    firebase.initializeApp(config);
  }

  render() {
    const { ContainerStyle, HeaderTextStyle, DescriptionText } = styles;
    return (
      <Container style={ContainerStyle}>
        <Text style={HeaderTextStyle}> All in one app </Text>
        <Image
          source={require("../../Assets/ic_launcher.png")}
          style={{ height: 100, padding: 16 }}
          resizeMode="contain"
        />
        <Text style={DescriptionText}>
          {" "}
          One stop solution for saving mobile memory and data{" "}
        </Text>
        <Item stackedLabel style={{ width: 300 }}>
          <Label>Mobile number</Label>
          <Input
            keyboardType={"number-pad"}
            onChangeText={Text => {
              this.setState({ number: Text });
            }}
          />
        </Item>
        <View
          style={{
            padding: 16,
            flexDirection: "row",
            alignItems: "flex-end",
            width: 320
          }}
        >
          <CheckBox
            checked={this.state.checkBoxStatus}
            color="black"
            onPress={() => {
              this.state.checkBoxStatus == false
                ? this.setState({ checkBoxStatus: true })
                : this.setState({ checkBoxStatus: false });
            }}
          />
          <Body>
            <Text style={{ textAlign: "left" }}>
              I Agree to the{" "}
              <Text
                style={{ fontWeight: "600", textDecorationLine: "underline" }}
              >
                Terms and Condition
              </Text>
            </Text>
          </Body>
        </View>
        <View style={{ alignItems: "flex-end", width: 300 }}>
          <Button
            style={{
              backgroundColor: "#fff",
              paddingLeft: 16,
              paddingRight: 16,
              alignSelf: "flex-end"
            }}
            onPress={() => {
              this.onSubmittButton();
            }}
          >
            <Title style={{ color: "#13C8B1" }}>Start</Title>
          </Button>
        </View>
      </Container>
    );
  }

  onSubmittButton() {
    if (this.isAllCondtionSatisfied()==true) {
      StoreUserPhoneNumber(this.state.number)
        .then(result => {
          firebase.database().ref('/Users').push({
            PhoneNumber: this.state.number
          })
          this.props.navigation.navigate("Navigator");
        })
        .catch(err => {});
    }
  }
  
  isAllCondtionSatisfied() {
    if (this.state.number != "" && this.state.checkBoxStatus == true) {
      return true;
    }
    return false;
  }
}

const styles = StyleSheet.create({
  ContainerStyle: {
    alignItems: "center",
    backgroundColor: "#13C8B1"
  },
  HeaderTextStyle: {
    marginTop: 20,
    fontSize: 40,
    fontWeight: "600",
    padding: 16,
    color: "#FFFFFF"
  },
  DescriptionText: {
    fontSize: 25,
    fontWeight: "400",
    padding: 16,
    textAlign: "center",
    color: "#FFFFFF"
  }
});
