import React, { Component } from "react";
import { Image, Text, View } from "react-native";
import GridViewProvider from "./GridViewProvider";
import { shoppingData } from "../Data/ShoppingData";
import { TraverlsData } from "../Data/TravelsData";
import { WomensData } from "../Data/WomensData";
import { RechargeData } from "../Data/RechargeData";
import { MovieData } from "../Data/MovieData";
import { BabyAndKidsData } from "../Data/BabyAndKidsData";
import { HotelsData } from "../Data/HotelsData";
import { FoodData } from "../Data/FoodData";
import { Button, Title } from "native-base";
import { GroceryData } from "../Data/GroceryData";
import { MedicineData } from "../Data/MedicineData";
import { FurnitureData } from "../Data/FurnitureData";
import { ClassifiedData } from "../Data/ClassifiedData";
import { HomeServiceData } from "../Data/HomeServiceData";
import { CarData } from "../Data/CarData";
import { FinanceData } from "../Data/FinanceData";
import { PrintiningData } from "../Data/PrintiningData";

const ButtonWithText = props => {
  return (
    <View style={{ flexDirection: "column", backgroundColor: "#f98" }}>
      <Button transparent style={{ height: 60 }}>
        <Image source={props.source} style={{ height: 30 }} />
        <Title style={{ color: "#000" }}>{props.name}</Title>
      </Button>
    </View>
  );
};
export class Shopping extends Component {
  render() {
    return (
      <View>
        <GridViewProvider
          Data={shoppingData}
          gridCellClicked={cell => {
            console.log("pressed", cell.Url);
            this.props.navigation.navigate("WebPresenter", {
              url: cell.Url,
              name: cell.text
            });
          }}
        />
      </View>
    );
  }
}

export class Womens extends Component {
  render() {
    return (
      <GridViewProvider
        Data={WomensData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Recharge extends Component {
  render() {
    return (
      <GridViewProvider
        Data={RechargeData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Movie extends Component {
  render() {
    return (
      <GridViewProvider
        Data={MovieData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class BabyAndKids extends Component {
  render() {
    return (
      <GridViewProvider
        Data={BabyAndKidsData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Hotel extends Component {
  render() {
    return (
      <GridViewProvider
        Data={HotelsData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Travel extends Component {
  render() {
    return (
      <GridViewProvider
        Data={TraverlsData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}
export class Food extends Component {
  render() {
    return (
      <GridViewProvider
        Data={FoodData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Grocery extends Component {
  render() {
    return (
      <GridViewProvider
        Data={GroceryData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Medicine extends Component {
  render() {
    return (
      <GridViewProvider
        Data={MedicineData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Furniture extends Component {
  render() {
    return (
      <GridViewProvider
        Data={FurnitureData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}
export class Classified extends Component {
  render() {
    return (
      <GridViewProvider
        Data={ClassifiedData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class HomeService extends Component {
  render() {
    return (
      <GridViewProvider
        Data={HomeServiceData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class car extends Component {
  render() {
    return (
      <GridViewProvider
        Data={CarData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Finance extends Component {
  render() {
    return (
      <GridViewProvider
        Data={FinanceData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}

export class Printing extends Component {
  render() {
    return (
      <GridViewProvider
        Data={PrintiningData}
        gridCellClicked={cell => {
          console.log("pressed", cell.Url);
          this.props.navigation.navigate("WebPresenter", {
            url: cell.Url,
            name: cell.text
          });
        }}
      />
    );
  }
}
