import React, { Component } from "react";
import { Text, View, Image, SafeAreaView } from "react-native";
import HeaderCommonComponent from "../Component/HeaderCommonComponent";
export default class ContactUsScreen extends Component {
  render() {
    return (
      <View>
        <HeaderCommonComponent
          navigation={this.props.navigation}
          name="Contact Us"
        />
        <View style = {{alignItems: 'center', paddingTop: 30, padding: 30}}>
          <Image
            style={{ height: 200, width: 200, borderRadius: 100 }}
            source={require("../../Assets/Icon.jpeg")}
          />
          <View style = {{height: 40}}/>
          <Text style = {{fontSize: 22, color: '#aaa', fontWeight: '300',  textAlign: "center", lineHeight: 30}}>
          We'd love to hear from you 
          <Text style = {{fontSize: 16}}>
           {"\n"+ " Weather you have any questions, Please drop a mail to "}
          </Text>
          <Text
            style={{
              fontSize: 18,
              color: "#000",
              textAlign: "center",
              fontWeight: "600",
              padding: 40
            }}
          >
            {" eyedeveloper67@gmail.com"}
          </Text>
          </Text>
        </View>
      </View>
    );
  }
}
