import React, { Component } from "react";
import { Text, View, SafeAreaView, StyleSheet } from "react-native";
import HeaderComponent from "../Component/HeaderCommonComponent";
import { ScrollView } from "react-native-gesture-handler";

export default class PrivecyPolicyScreen extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <HeaderComponent
          navigation={this.props.navigation}
          name="Privecy Policy"
        />
        <ScrollView style={{ flex: 0.9, padding: 16 }}>
          <Text style={styles.descirptionStyle}>
            We at “All in One Shoppin App” take your privacy seriously. This
            privacy statement explains what data we collect, how we store and
            transfer it and how we use it. The exact set of the data we collect,
            its purpose and your opt-out choices depend on the product or
            service you use. Please refer to the relevant product or service
            sections of this privacy statement to learn more specific details.
            /n /n Some of the data we collect, either individually or
            aggregated, may be considered personal. We treat this data in a
            special way, as required by law. {"\n"} We'll always honor your
            decisions for how your data is used. We have safeguards in place to
            protect your data from loss, misuse, and unauthorized access,
            disclosure, alteration, or destruction. {"\n"} When we post changes
            to this privacy statement, we will include the date when the
            statement was last updated. If we significantly change this
            statement or how we use your personal information, we will notify
            you either by prominently posting a notice prior to implementing the
            change or by sending you a notification directly. We encourage you
            to respect review this statement periodically.
          </Text>
          <Text style={[styles.headerStyle, { paddingTop: 20 }]}>
            Why do we collect data?
          </Text>
          <Text style={[styles.descirptionStyle, { paddingTop: 10 }]}>
            We only process your data for purposes that are objectively
            justified by our products and services. We process this data with
            respect to your fundamental right to privacy, including the need to
            protect personal integrity and private life and to ensure that your
            personal data is of adequate quality. Unless otherwise stated, we
            determine the purposes for and manners in which your data is
            collected (in legalese, we act as a "data controller" regarding
            personal data).
          </Text>
          <Text style={[styles.headerStyle, { paddingTop: 20 }]}>
            We collect data to:
          </Text>
          <Text style={[styles.descirptionStyle, { paddingTop: 10 }]}>
            1) Improve, debug, and maintain All in One Online shopping App
            products and services {"\n\n"} 2) Study and personalize user
            experiences {"\n\n"} 3) Fulfill legal requirements {"\n\n"} 4)
            Ensure better security and fraud protection
          </Text>
          <Text style={[styles.headerStyle, { paddingTop: 20 }]}>
            How do we collect data?
          </Text>
          <Text style={[styles.descirptionStyle, { paddingTop: 10 }]}>
            Generally, we collect data : {"\n\n"} 1. When you provide it
            explicitly to us; for example, when you submit a form on our
            application
          </Text>

          <Text style={[styles.headerStyle, { paddingTop: 20 }]}>
            How do we protect your data?
          </Text>
          <Text style={[styles.descirptionStyle, { paddingTop: 10 }]}>
            We treat your personal data as required by law. We are a Indian
            company and we follow Indian data-security laws as well as other
            national legislation, as needed. We require that our suppliers
            successfully pass security assessments and prove their compliance
            with applicable laws and industry standards. {"\n\n"} Only a limited
            number of employees have access to the data we collect. We review
            and update our working procedures regularly to improve your privacy
            and ensure that our internal policies are followed.
          </Text>
          <Text style={[styles.headerStyle, { paddingTop: 20 }]}>
            Privacy Policy Updates
          </Text>
          <Text style={[styles.descirptionStyle, { paddingTop: 10 }]}>
            Our Company, may update this Privacy Policy time to time. We will
            notify you of any changes by posting the new Privacy Policy on the
            Application. You are advised to review this Privacy Policy
            periodically for any changes
          </Text>
          <Text style={[styles.headerStyle, { paddingTop: 20 }]}>
            Contact Us
          </Text>
          <Text style={[styles.descirptionStyle, { paddingTop: 10 }]}>
            If you have any questions about this Privacy Policy.
            <Text style={[styles.headerStyle, {  }]}> {"\n"} please contact us. : eyedeveloper67@gmail.com</Text>
          </Text>
          <Text
            style={[
              styles.headerStyle,
              { paddingTop: 20, height: 60, paddingBottom: 20 }
            ]}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  descirptionStyle: {
    fontSize: 16,
    color: "#000",
    lineHeight: 22
  },
  headerStyle: {
    fontSize: 18,
    color: "#000",
    fontWeight: "600"
  }
});
