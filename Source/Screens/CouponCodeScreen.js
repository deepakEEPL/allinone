import React, { Component } from "react";
import { Text, View, ScrollView } from "react-native";
import HeaderComponent from "../Component/HeaderCommonComponent";
import CouponCodeListViewProvider from "../Component/CouponCodeListViewProvider";
import { Button, Title } from "native-base";
import Webpresenter from "../Router/WebPresenter";
import SearchPresenter from "./SearchPresenter";
import {
  createAppContainer,
  createStackNavigator,
  DrawerActions
} from "react-navigation";
import HeaderCommonComponent from "../Component/HeaderCommonComponent";
const CouponCodeScreen = props => {
  return (
    <View style={{ flex: 1, backgroundColor: "#E7E7E7" }}>
      <ScrollView>
        <View style={{ height: 300 }}>
          <CouponCodeListViewProvider navigationvalue={props.navigation} />
        </View>
      </ScrollView>
    </View>
  );
};

export default  CouponCodeStackNavigator = createStackNavigator({
  CouponCodeScreen: {
    screen: CouponCodeScreen,
    navigationOptions: ({ navigation }) => ({
      header: (
        <HeaderCommonComponent navigation={navigation} name="Coupon code" />
      )
    })
  },
  WebPresenter: { screen: Webpresenter },
  SearchPresenter: { screen: SearchPresenter }
});


