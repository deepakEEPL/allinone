import React, { Component } from "react";
import { View, Image } from "react-native";
import Carousel, { Pagination } from "react-native-snap-carousel";
import { Text, Button, Title, Icon } from "native-base";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Navigator from "../Router/Navigator";
import { TabNavigator } from "../Router/TabNavigator";
import DrawerNavigator from "../Router/DrawerNavigator";
import UserDetailsScreen from "./UserDetailsScreen";
const data = [
  {
    name: "Save Memory",
    description:
      "You saved 12GB of data by installing this app. No need to update individual apps",
    color: "#13C8B1",
    image: require("../../Assets/Savememory.png")
  },
  // {
  //   name: "News and live TV",
  //   description:
  //     "Read News, Coupons and Deals. Listen music with additional software tools",
  //   color: "#F9CD4E",
  //   image: require("../../Assets/news.png")
  // },
  {
    name: "Daily offer and coupons",
    description: "You will get automatically notification about upcoming sale",
    color: "#54499D",
    image: require("../../Assets/Sale.png")
  }
];
class WelcomeScreen extends Component {
  state = { pageWidth: 1, pageHeight: 1 };
  constructor(props) {
    super(props);
    this.state = {
      slider1ActiveSlide: 0,
      pageWidth: 1,
      pageHeight: 1
    };
  }

  _renderItem({ item, index }) {
    const { name, description, color, image } = item;
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: color,
          alignItems: "center",
          padding: 32,
          justifyContent: "center"
        }}
      >
        <Text style={{ marginTop: 80, fontSize: 36, color: "#fff" }}>
          {name}
        </Text>
        <Image source={image} style={{ height: 300 }} resizeMode="contain" />
        <Text style={{ marginBottom: 80, fontSize: 24, color: "#fff" }}>
          {description}
        </Text>
      </View>
    );
  }

  getPagination() {
    return (
      <Pagination
        dotsLength={2}
        activeDotIndex={this.state.slider1ActiveSlide}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          backgroundColor: "#000"
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
        parallaxFactor={0.1}
      />
    );
  }

  getCarousel() {
    return (
      <Carousel
        data={data}
        renderItem={this._renderItem.bind(this)}
        onSnapToItem={index => this.setState({ slider1ActiveSlide: index })}
        sliderWidth={this.state.pageWidth}
        itemWidth={this.state.pageWidth}
        itemHeight={this.state.pageHeight}
        parallaxFactor={0.1}
      />
    );
  }

  render() {
    return (
      <View
        style={{ flex: 1 }}
        onLayout={event => {
          this.setState({
            pageWidth: event.nativeEvent.layout.width,
            pageHeight: event.nativeEvent.layout.height
          });
        }}
      >
        {this.getCarousel()}
        <View
          style={{
            height: 1,
            backgroundColor: "#aaa",
            width: this.state.pageWidth
          }}
        />
        <View
          style={{
            padding: 10,
            flexDirection: "row",
            width: this.state.pageWidth,
            justifyContent: "flex-end",
            alignItems: "center"
          }}
        >
          <View style={{ width: this.state.pageWidth * 0.3 }} />
          {this.getPagination()}
          <Button
            transparent
            style={{
              width: this.state.pageWidth * 0.4,
              justifyContent: "flex-end",
              alignItems: "flex-end"
            }}
            onPress={() => {
              console.log(this.props.navigation);
              this.props.navigation.navigate("UserDetailsScreen");
            }}
          >
            <Icon
              name={"arrow-dropright"}
              style={{ fontSize: 30, color: "#000", alignSelf: "flex-end" }}
            />
          </Button>
        </View>
      </View>
    );
  }
}
const stackNavigator = createStackNavigator(
  {
    welcomeScreen: { screen: WelcomeScreen },
    UserDetailsScreen: { screen: UserDetailsScreen },
    Navigator: { screen: Navigator }
  },
  { headerMode: "none" }
);

export default createAppContainer(stackNavigator);
