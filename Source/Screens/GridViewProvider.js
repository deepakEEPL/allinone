import React, { Component } from "react";
import { Text, View } from "react-native";
import GridView from "react-native-easy-grid-view";
import CellComponent from "../Component/CellComponent";

export default class GridViewProvider extends Component {
  constructor(props) {
    super(props);
    
  }

  _renderCell(cell) {
    return <CellComponent cellName={cell.text} image={cell.image} onCellClicked = {()=> {this.props.gridCellClicked(cell)}}/>;
  }
  render() {
    var ds = new GridView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    this.state = {
      dataSource: ds.cloneWithCells(this.props.Data, 2)
    };
    return (
      <View style={{ paddingBottom: 8, backGroundColor: "#E6E6E6" }}>
        <GridView
          dataSource={this.state.dataSource}
          spacing={16}
          style={{ padding: 16 }}
          renderCell={this._renderCell.bind(this)}
        />
      </View>
    );
  }
}
