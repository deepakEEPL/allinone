import React, { Component } from "react";
import { Text, View } from "react-native";
import { shoppingData } from "../Data/ShoppingData";
import { TraverlsData } from "../Data/TravelsData";
import { WomensData } from "../Data/WomensData";
import { RechargeData } from "../Data/RechargeData";
import { MovieData } from "../Data/MovieData";
import { BabyAndKidsData } from "../Data/BabyAndKidsData";
import { HotelsData } from "../Data/HotelsData";
import { FoodData } from "../Data/FoodData";
import { GroceryData } from "../Data/GroceryData";
import { MedicineData } from "../Data/MedicineData";
import { FurnitureData } from "../Data/FurnitureData";
import { ClassifiedData } from "../Data/ClassifiedData";
import { HomeServiceData } from "../Data/HomeServiceData";
import { CarData } from "../Data/CarData";
import { FinanceData } from "../Data/FinanceData";
import { PrintiningData } from "../Data/PrintiningData";
import GridViewProvider from "../Screens/GridViewProvider";
import HeaderCommonComponent from "../Component/HeaderCommonComponent";
import { ScrollView, TextInput, State } from "react-native-gesture-handler";
import { Button, Title, Row } from "native-base";
const dataValues = [].concat.apply(
  [],
  [
    shoppingData,
    TraverlsData,
    WomensData,
    RechargeData,
    MovieData,
    BabyAndKidsData,
    HotelsData,
    FoodData,
    GroceryData,
    MedicineData,
    FurnitureData,
    ClassifiedData,
    HomeServiceData,
    CarData,
    FinanceData,
    PrintiningData
  ]
);
export default class SearchPresenter extends Component {
  state = { dataArray: dataValues };
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <View style={{ flexDirection: "row", padding: 10 }}>
          <TextInput
            placeholder={"Search here"}
            style={{ paddingLeft: 20, paddingRight: 20, width: 300 }}
            onChangeText={text => {
              this.setState({ text });
              this.getFilteredArrayValues(text);
            }}
            value={this.state.text}
          />
        </View>
        <ScrollView>
          <GridViewProvider
            Data={this.state.dataArray}
            gridCellClicked={cell => {
              console.log("pressed", cell.Url);
              this.props.navigation.navigate("WebPresenter", {
                url: cell.Url,
                name: cell.text
              });
            }}
          />
        </ScrollView>
      </View>
    );
  }

  getFilteredArrayValues(filterString) {
    data = dataValues.filter(element =>
      element.text.toLowerCase().includes(filterString.toLowerCase())
    );
    this.setState({ dataArray: data });
  }
}
