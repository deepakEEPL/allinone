import React, { Component } from "react";
import { Text, View, SafeAreaView } from "react-native";
import HeaderCommonComponent from "../Component/HeaderCommonComponent";
import CategoryListProvider from "../Component/CategoryListProvider";
import { Container } from "native-base";
import Webpresenter from "../Router/WebPresenter";
import SearchPresenter from "./SearchPresenter";
// const  list1 = [{header: "Women's shopping" , subHeader: "shopping"},{header: "Men and Women" , subHeader: "shopping"}]
import {
  createAppContainer,
  createStackNavigator,
  DrawerActions
} from "react-navigation";
const  CategoryScreen = props => {
  return (
    <Container>
      <View style = {{flex: 0.4, marginTop: 60}}>
      <CategoryListProvider nameValue = {1} navigationvalue = {props.navigation}/>
      </View>

      <View style = {{flex: 0.4, paddingTop: 60}}>
      <CategoryListProvider nameValue = {0} navigationvalue = {props.navigation}/>
      </View>
    </Container>
  )
} 


export default  CategoryScreenStackNavigator = createStackNavigator({
  CouponCodeScreen: {
    screen: CategoryScreen,
    navigationOptions: ({ navigation }) => ({
      header: (
        <HeaderCommonComponent navigation={navigation} name="Category" />
      )
    })
  },
  WebPresenter: { screen: Webpresenter },
  SearchPresenter: { screen: SearchPresenter }
});

