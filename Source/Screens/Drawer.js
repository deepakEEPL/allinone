import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import { Button, Icon, Title, Header } from "native-base";
import IntroComponent from "../Component/IntroComponent";
import { TouchableHighlight } from "react-native-gesture-handler";
import { DrawerActions, DrawerItems } from "react-navigation";
import { Dialog } from "react-native-simple-dialogs";
import Modal from "react-native-modal";
import { AirbnbRating } from "react-native-ratings";
const ButtonWithTitle = props => {
  return (
    <Button transparent onPress={props.onButtonPressed}>
      <Icon name={props.iconName} style={{ color: "#fff" }} />
      <Title style={{ color: "#fff" }}>{props.titleName}</Title>
    </Button>
  );
};

export default class Drawer extends Component {
  constructor() {
    super();
    this.state = {
      pageWidth: 1,
      rateVisible: false
    };
  }
  render() {
    return (
      <View
        style={{ backgroundColor: "#846CFF", flex: 1 }}
        onLayout={event => {
          this.setState({
            pageWidth: event.nativeEvent.layout.width
          });
        }}
      >
        {this.getAlert()}
        <View
          style={{
            height: 60,
            backgroundColor: "#846CFF",
            paddingLeft: 24,
            justifyContent: "space-between",
            alignItems: "center",
            backgroundColor: "#fff",
            flexDirection: "row"
          }}
        >
          <Title
            style={{ height: 40, color: "#846CFF", alignSelf: "flex-end" }}
          >
            Menu
          </Title>
          <Button
            transparent
            style={{ height: 40, alignSelf: "flex-end" }}
            color="#000"
            onPress={() => {
              this.props.navigation.dispatch(DrawerActions.toggleDrawer());
            }}
          >
            <Icon name="close" style={{ color: "#000" }} />
          </Button>
        </View>
        <IntroComponent name="All in one shopping" />
        <DrawerItems {...this.props} />
        <Button
          transparent
          style={{
            height: 50,
            width: this.state.pageWidth,
            justifyContent: "flex-start",
            paddingLeft: 14
          }}
          color="#000"
          onPress={() => {
            this.setState({ rateVisible: true });
            this.props.navigation.dispatch(DrawerActions.toggleDrawer());
          }}
        >
          <Image
            source={require("../../Assets/ic_rate.png")}
            style={{ height: 30, width: 30 }}
          />
          <Title
            style={{
              textAlign: "left",
              color: "#eee",
              paddingLeft: 24,
              fontSize: 16
            }}
          >
            Rate App
          </Title>
        </Button>
      </View>
    );
  }

  getAlert() {
    return (
      <Modal isVisible={this.state.rateVisible}>
        <View
          style={{
            paddingTop: 40,
            alignItems: "center",
            padding: 16,
            backgroundColor: "#fff"
          }}
        >
          <Text style={{ fontSize: 30, fontWeight: "600", padding: 10 }}>
            Rate out App
          </Text>
          <Text style={{ fontSize: 18, fontWeight: "600", paddingBottom: 30 }}>
            Let us know how we're doing. Please rate your experince using All in
            One Shopping App
          </Text>
          <AirbnbRating showRating={false} />
          <View style={{ height: 40 }} />
          <Button
            transparent
            style={{ alignSelf: "stretch", justifyContent: "center" }}
            onPress={() => {
              this.setState({ rateVisible: false });
              this.props.navigation.dispatch(DrawerActions.toggleDrawer());
            }}
          >
            <Title style={{ color: "#000" }}>OK</Title>
          </Button>
        </View>
      </Modal>
    );
  }
}
