/** @format */

import {AppRegistry} from 'react-native';
import App from './Source/Router/Navigator';
import {name as appName} from './app.json';
import WelcomeScreen from './Source/Screens/WelcomeScreen';
import SearchPresenter from './Source/Screens/SearchPresenter';
import InitalPage from './Source/Screens/Initalpage';

AppRegistry.registerComponent(appName, () => InitalPage);
